#################################### Q1 ####################################
data <- mtcars


summary(data$mpg)
 
#convert mpg column to factor
data$mpg <- as.factor(data$mpg)



#הש וותיק
conv_YESNO <-function(x){
  x <-ifelse(x>19.2,1,0)
  x <- factor(x,level=c(1,0), labels =c('Yes','No'))
}


data[1:1] <- apply(data[1:1],MARGIN=1:2,conv_YESNO)

summary(data[1:1])


######################### Q2 #######################################

#spliting to training set and test set
split <- runif(nrow(data))>0.3

dataTrain <- data[split,]#70%
dataTest <- data[!split,]#30%

install.packages('rpart')#מכיל את אלגוריתם עץ החטות
library(rpart)

install.packages('rpart.plot')#מציג את האלגוריתם בצורת עץ
library(rpart.plot)


str(data)


#convert mpg column to factor
data$mpg <- as.factor(data$mpg)

#data$cyl <- as.factor(data$cyl)
#data$vs <- as.factor(data$vs)
#data$am <- as.factor(data$am)
#data$gear <- as.factor(data$gear)
#data$carb <- as.factor(data$carb)
#data$wt <- as.factor(data$wt)

#compute the decision tree model
dcModel <- rpart(mpg ~ cyl + disp + hp + drat + wt + qsec
                 + vs + am + gear + carb , data=dataTrain, method = "class")

#תצוגה של העץ
rpart.plot::rpart.plot(dcModel, fallen.leaves = TRUE)

############################# Q3 #############################
data <- mtcars
library(class)
str(data)

#לא צריך פה להפוך לנומרי כי כבר הכל נומרי מהמקורי, צריכים להפוך רק שעשינוי שינוי בתרגילים קודמים
#Turn 'mpg' from factor to numeric field
#data$mpg <- as.numeric(data$mpg)
#data$wt <- as.numeric(data$wt)
#sub df from data

sub_set <- mtcars[,1:7]

#Make scaling on the numeric fields NIRMUL!!!
scale_num <- scale(sub_set)
str(scale_num)

boxplot(sub_set, col = "GREEN")
boxplot(scale_num, col = "GREEN")

#switch between the scale_num fileds and the mtcars fileds
mtcars[,1:7] <- scale_num

#example for subset NEED HEARE
mtcars_subset <- mtcars[,c(1,6)]

set.seed(111111)

#applying the KMEANS model. 4=K 
clusters <- kmeans(mtcars_subset,4)

#vizualization of the data
plot(mtcars_subset$mpg,mtcars_subset$wt,  col = clusters$cluster, pch = 20, cex = 2)
points(clusters$centers, col = 'purple', pch = 17, cex = 3)

clusters$iter
clusters$size

################################# עד כאן אפשר לענות על השאלה ########3
install.packages('mlr')

library(mlr)

df <- data.frame(var = sample(c("A","B","C"), 10, replace =TRUE))
createDummyFeatures(df, cols="var")

#יודע לעבוד רק על פקטורים הגדרת משתנה דמה
#נהפוך את המשקל לפקטור
data$wt <- as.factor(data$wt)
#convert mpg column to factor
data$mpg <- as.factor(data$mpg)
subsetAsFactorMPGWT <- data[1:32, c(1,6)]

str(subsetAsFactorMPGWT)
 

auto_data_dum <- createDummyFeatures(subsetAsFactorMPGWT, cols = names(subsetAsFactorMPGWT)[1:2])
clusters <- kmeans(auto_data_dum,15)



########################  Q4  ##############################
data <- mtcars
str(data)

cutMpg <- cut(data$mpg, breaks = c(0,19.2,100) ,labels=c("Low-Mpg","High-Mpg"))
data$mpg <- cutMpg

cutCyl <- cut(data$cyl, breaks = c(0,4,6,8) ,labels=c("4Cyl","6Cyl","8Cyl"))
data$cyl <- cutCyl

cutDisp <- cut(data$disp, breaks = c(70,230.7,326,473) ,labels=c("DIS-LOW","DIS-CENTER/HIGH","DIS-TOP"))
data$disp <-  cutDisp

cutAm <- cut(data$am, breaks = c(-1,0,1) ,labels=c("automatic","manual"))
data$am <- cutAm

cutHP <- cut(data$hp, breaks = c(0,100,200,350) ,labels=c("(0-100)HP","(100-200)HP","(200-350)HP"))
data$hp <- cutHP

cutDRAT <- cut(data$drat, breaks = c(0,3,4,5) ,labels=c("weak","good","best"))
data$drat <- cutDRAT

cutWT <- cut(data$wt, breaks = c(1,2,3,6) ,labels=c("light","normal","heavy"))
data$wt <- cutWT

cutqsec <- cut(data$qsec, breaks = c(14,20,23) ,labels=c("OkQsec","TopQsec"))
data$qsec <- cutqsec

cutVS <- cut(data$vs, breaks = c(-1,0,1) ,labels=c("V","S"))
data$vs <- cutVS

cutGear <- cut(data$gear, breaks = c(0,3,4,5) ,labels=c("3-GEAR","4-GEAR","5-GEAR"))
data$gear <- cutGear

cutcarb <- cut(data$carb, breaks = c(0,2,4,9) ,labels=c("F-CARBON","F/S-CARBON","F/S/R-CARBON"))
data$carb <- cutcarb

#if i want new df
#newDFdata <- data.frame(cutMpg, cutCyl, cutDisp,cutHP,cutAm,cutDRAT,cutWT,cutqsec,cutVS,cutGear,cutcarb)
#names(newDFdata) <- c("MPG", "CYL", "Disp", "HP", "A/M","Drat", "Weight", "Qsec","V/S","Gear","Carbon")

####################### Q5 ##################################
install.packages("arules")
library('arules')


#write.table(data,file = 'newDFdata33.csv', sep =',',col.names = FALSE, row.names = FALSE)
#transactions <- read.transactions('newDFdata2.csv', sep = ',')

#data = as(data, "transactions");

#itemFrequencyPlot(Adult, topN = 20, type = 'absolute', col = "darkgreen", horiz = TRUE)

#Applying rules
rules <- apriori(data, parameter = list(supp = 0.01, conf = 0.4, maxlen=4))

inspect(rules)

#sort the rules
rules<-sort(rules, by="confidence",decreasing=TRUE)
inspect(rules)
inspect(head(rules))

rules<-sort(rules, by="lift",decreasing=TRUE)
inspect(head(rules))

rules<-sort(rules, by="support",decreasing=TRUE)
inspect(head(rules))

#דוגמא לגיט


